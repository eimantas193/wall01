#include <stm32l4xx_hal.h>
#include <stm32l4xx_hal_usart.h>
#include "string.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h> 


volatile uint8_t buffer[30];
volatile uint8_t data[100];
volatile uint8_t answer[5];
int step = 0;
volatile int buffer_recieved = 0;
volatile uint8_t temp = 0;
int time_ms = 1;
int max_time_ms = 1;
int flag = 0;
int cr=0;

void solution();
int main(void)
{

    GPIO_InitTypeDef GPIO_InitStructure;

    //Enable UART GPIO PINS
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN; 
    GPIO_InitStructure.Pin = GPIO_PIN_4;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Alternate = GPIO_AF7_USART3;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

    GPIO_InitStructure.Pin = GPIO_PIN_5;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_OD;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);


    //UART Setup
    //0x65B9A
    RCC->APB1ENR1 |= RCC_APB1ENR1_USART3EN;
    USART3->BRR |= 0x1A0;             // check clock frq RCC_CR reset value 4 MHz MSI clock [7:4] [0] registers
    USART3->CR1 |= USART_CR1_UE;     // UART enable
    USART3->CR1 |= USART_MODE_TX_RX; // UART mode bidirectional
    USART3->CR1 |= USART_CR1_TE;     // Transfer enebale
    USART3->CR1 |= USART_CR1_RE;     // receive enable
    USART3->CR1 |= USART_CR1_RXNEIE; // Receive interrupt enable/ event - Receive data register not empty (data ready to be read)
    USART3->CR1 |= USART_STOPBITS_1;
    USART3->CR1 |= USART_PARITY_NONE;
    USART3->CR1 |= USART_WORDLENGTH_8B;
    //USART3->CR1 |= USART_OVERSAMPLING_8;
    //USART2->CR1 |= USART_CR1_TXEIE;  // transfer interrupt enable/ event - Transmit data register empty
    NVIC_EnableIRQ(USART3_IRQn);
    int i=0;
    while (1)
    {
        if (buffer_recieved) //buffer_recieved
            {
            solution();
            i=sizeof(answer)-1;
            while (i>=0)
                {
                    if (answer[i]=='\0') //ship empty spaceses 
                    {
                        i--;
                        continue;
                    } 
                    if ((USART3->ISR & USART_ISR_TXE) && (USART3->ISR & USART_ISR_TC))
                    {
                        USART3->TDR = (uint8_t *)answer[i]; //sends HEX one by one
                        i--;
                    }
                }
              
            buffer_recieved = 0;  
            i=0;
            memset(answer, '\0', sizeof(answer)); // cleans answer buffer
        } 
    }
    
    
}

void USART3_IRQHandler(void)
{

    if (USART3->ISR & USART_ISR_RXNE) // check RX not empty RXNE (RXNEIE interrupt) data ready to be read
    {
        temp = USART3->RDR;
        if (temp == 0x0d || cr==1) //CR
        {
            cr=1;
            if (temp == 0x0a) // LF
            {
            buffer_recieved = 1;
            step = 0;
            cr=0;
            }
            
        }
        else
        {
            
            buffer[step] = temp;
            step++;
        }
    }
    if (USART3->ISR & USART_ISR_ORE)
    {
        USART3->ICR |= USART_ICR_ORECF;
    }
}

void solution()
{
    int sk_flag=0, s=0, z=0, f=0, flag=0, save;
    volatile uint8_t int_buffer[10];
    volatile float int_arry[10], dis;
    volatile int half_answer;
    volatile uint8_t sign_arry[10];
        for(int j=0; j<sizeof(buffer); j++)
        {
            if(buffer[j] == '-'|| buffer[j] == '+'|| buffer[j] == '*'|| buffer[j] == '/' || buffer[j] == 'x'||buffer[j] == '='||buffer[j] == '('||buffer[j] == ')'||buffer[j] == '^')
            {   
                if (buffer[j] == '^') flag=1; //checks if equation is ('x^2')
                if (sk_flag==1) // fuse saved numbers into one from buffer 
                {
                        int_arry[f] = atoi((char *)int_buffer);
                        memset(int_buffer, '\0', sizeof(int_buffer)); //clears buffer for new calculations
                        sk_flag=0;
                        s=0;
                        f++;
                }
                sign_arry[z]=buffer[j];
                z++;
            }
            else 
            {
                sk_flag=1; // marks that readers just read a digit
                int_buffer[s]=buffer[j];
                s++;
            } 
            if(buffer[j] == '\0') break; // checks if it's the end of equation 
            
        }
        if (flag==0) //'x' equations are sloved here
        {
            int i=0;
            int xcount=0;
            int division=0;
            while(i<z)
            {
                if (sign_arry[i]=='=') break;
                if(sign_arry[i]=='x')// checks how many x are for further calculations
                {
                    xcount++;
                }
            if(sign_arry[i]=='/') division=1;// looks for division sign
            
            i++;
            }
            if (xcount>=2)
            {
                half_answer=round(int_arry[2]/(int_arry[0]+(1/int_arry[1]))); // calculating final answer when their are two x
            }
            else if (xcount<2 && division==1)
            {
                half_answer=round(int_arry[1]/(1/int_arry[0])); // calculating final answer with 'x/numb'
            }
            else half_answer=round(int_arry[1]/int_arry[0]); // calculating final answer when their is one x
            
            
        }
        if (flag==1) //'x^2' equations are sloved here
        {
            volatile float int_x2arry[10];
            int x_flag=0;
            int x2_flag=0;
            int j=0;
            int_x2arry[2]=int_arry[f-1];
            int i=0;
            while(i<z)
            {
                if (sign_arry[i]=='=') break;
                if(sign_arry[i]=='x')
                {
                    if(sign_arry[i+1]=='/')
                    {
                        if(x_flag==0)
                        {
                            int_x2arry[1]=1/int_arry[j];
                        }
                        else
                        {
                            int_x2arry[1]=int_x2arry[1]+(1/int_arry[j]);
                            int_arry[j]=int_arry[j+1];
                            j--;
                        }
                            x_flag=1;
                            i=i+2;
                            j++;
                    }
                    else
                    {
                        if(x_flag==0)
                        {
                            int_x2arry[1]=int_arry[j];
                        }
                        else
                        {
                            int_x2arry[1]=int_x2arry[1]+int_arry[j];
                            int_arry[j]=int_arry[j+1];
                            j--;
                        }
                            x_flag=1;
                            i++;
                            j++;
                        
                    }
                    continue;
                }
                else if(sign_arry[i]=='(')
                { 
                    i=i+3;
                    if(sign_arry[i+1]=='/')
                    {
                        
                        if(x2_flag==0)
                        {
                            int_x2arry[0]=1/int_arry[j];
                        }
                        else 
                        {
                            int_x2arry[0]=int_x2arry[0]+(1/int_arry[j]);
                            int_arry[j]=int_arry[j+1];
                            j--;
                        }
                        i=i+2;
                        j=j+2;
                        x2_flag=1;
                    }
                    else
                    {
                        if(x2_flag==0)
                        {
                        int_x2arry[0]=int_arry[j];
                        }
                       else
                       {
                        int_x2arry[0]+=int_arry[j];
                        int_arry[j]=int_arry[j+1];
                        j--;
                       }
                        i++;
                        j=j+2;
                        x2_flag=1; 
                    }
                    
                    continue;
                }
                else 
                {
                    i++;
                    continue;
                }
                
            }
            dis=pow(int_x2arry[1],2)+4*int_x2arry[0]*int_x2arry[2]; //calculating discriminat
            
            half_answer=round((sqrt(dis)-int_x2arry[1])/(2*int_x2arry[0]));// calculating final answer
        }
        int i=0;
        while(half_answer!=0) // converts decimal number to hex ASCII
        {
            answer[i] = 0x30|half_answer%10; //ASCII conversion
            half_answer=half_answer/10;
            i++;
        }
}
